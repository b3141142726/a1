// Use an object literal: {} to create an object representing a user
	//encapsulation
	// Whenever we add properties or methods to an object, we are performing ENCAPSULATION
	// the organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
	// (the scope of encapsulation is denoted by object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	// methods
		// add the functionalities available to a student as object methods
		// the keyword "this" refers to the object encapsulating the method where "this" is called

	login(){
		console.log(`${this.email} has logged in`)
	},
	logout(){
		console.log(`${this.email} has logged out`)
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are ${this.grades}`)
	},

	// Mini-activity
		// Create a function that will get a quarterly average of studentOne's grades
	getQuarterlyAverage() {
	    const sum = this.grades.reduce((total, grade) => total + grade, 0);
	    return sum / this.grades.length;
	  },

	// Mini-activity 2
		// Create a function that will return true if average grade is >=85, false otherwise
	  willPass(){
	  	const average = this.getQuarterlyAverage();
	  	return average >= 85;
	  },

	// Mini-activity 3
		// Create a function called willPassWithHonors() that returns true if the student has passed and their average is >=90. The function returns false if either one is not met.
	  willPassWithHonors(){
	  	const average = this.getQuarterlyAverage();
	  	return this.willPass() && average >= 90;
	  }

}






// Activity S02

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	getQuarterlyAverage() {
	    const sum = this.grades.reduce((total, grade) => total + grade, 0);
	    return sum / this.grades.length;
	  },

	willPass(){
		const average = this.getQuarterlyAverage();
		return average >= 85;
	},
	willPassWithHonors(){
	  	const average = this.getQuarterlyAverage();

	  	if(average >= 90){
	  		return true;
	  	}
	  	else if(average >=85 && average < 90){
	  		return false;
	  	}
	  	else{
	  		return undefined;
	  	}
	  }
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	getQuarterlyAverage() {
	    const sum = this.grades.reduce((total, grade) => total + grade, 0);
	    return sum / this.grades.length;
	  },

	willPass(){
		const average = this.getQuarterlyAverage();
		return average >= 85;
	},
	willPassWithHonors(){
	  	const average = this.getQuarterlyAverage();

	  	if(average >= 90){
	  		return true;
	  	}
	  	else if(average >=85 && average < 90){
	  		return false;
	  	}
	  	else{
	  		return undefined;
	  	}
	  }
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	getQuarterlyAverage() {
	    const sum = this.grades.reduce((total, grade) => total + grade, 0);
	    return sum / this.grades.length;
	  },

	willPass(){
		const average = this.getQuarterlyAverage();
		return average >= 85;
	},
	willPassWithHonors(){
	  	const average = this.getQuarterlyAverage();

	  	if(average >= 90){
	  		return true;
	  	}
	  	else if(average >=85 && average < 90){
	  		return false;
	  	}
	  	else{
	  		return undefined;
	  	}
	  }
}

let classOf1A = {
	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		let count = 0;

		for(let student of this.students){
			if(student.willPassWithHonors()){
				count++;
			}

		}
		return count;
	},
	honorsPercentage(){
		const count = this.countHonorStudents();
		const totalStudents = this.students.length;

		const honorStudentPercentage = (count / totalStudents) * 100;
		return honorStudentPercentage;
	},
	retrieveHonorStudentInfo(){
		const honorStudentsInfo = [];

		for(let student of this.students){
			if(student.willPassWithHonors()){
				const studentInfo = {
					email: student.email,
					average: student.getQuarterlyAverage()
				};

				honorStudentsInfo.push(studentInfo)
			}
		}

		return honorStudentsInfo;
	},
	sortHonorStudentsByGradeDesc(){
		const honorStudentsInfo = this.retrieveHonorStudentInfo();

		honorStudentsInfo.sort((a, b) => b.average - a.average);

		return honorStudentsInfo;
	}
}



// log the content of studentOne's encapsulated information in the console
console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade averages are ${studentOne.grades}`);
console.log(studentOne);

